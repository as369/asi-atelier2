import CardList from "../components/cardList.js"
import Card from "../components/card.js"
import SaleService from "../services/saleService.js";
import HTMLBindableElement from  "../components/abstract/HTMLBindableElement.js";
import saleService from "../services/saleService.js";
import userService from "../services/userService.js";

class BuyView extends HTMLBindableElement {
	constructor() {
		super();
	}
	
	async getAllSellCards() {
		const response = await saleService.getCardsToSell();
		const sells = _.get(response, 'data');
		return sells;
	}
	
	async render() {
		const sells = await this.getAllSellCards();
		this.cards = sells;
		
		this.innerHTML = `
		<div id="container" class="ui grid vertical segment">
			<div id="cardListContainer" class="twelve wide column"></div>
			<div id="cardContainer" class="four wide column"></div>
		</div>
		`;

		this.cardList = new CardList(this.cards, 'buy', (card) => this.selectCard({ card }));
		$("#cardListContainer").append(this.cardList);
		this.card = new Card({}, 'buy');
		this.card.bindOnButtonClick(() => this.buy())
		$("#cardContainer").append(this.card);
	}
	
	selectCard(card) {
		this.card.setCard(card)
	}
	
	async buy() {
		try {
			const idCardInstance = _.get(this.card, 'card.id_instance');
			await SaleService.buyCard({ idCardInstance });
			this.render();
		} catch (errors) {
			throw new Error(errors);
		} 
	}
}

customElements.define('buy-view', BuyView);