import userService from "../services/userService.js";
import HTMLBindableElement from "./abstract/HTMLBindableElement.js";
import redirectUrl from '../router/router.js';

class Header extends HTMLBindableElement {
    constructor() {
        super();
        window.Components = {
            header: this
        };
    }
    
    render() {
        this.userActions = `
            <div>
                <i class="user circle outline icon"></i>
                <div class="content">
                    <span id="userNameId">Jdoe</span>
                    <div class="sub header">
                        <span>5000</span>
                        $
                    </div>
                </div>
            </div>
            <div class="ui item" onclick="${this.bind(() => this.clearUser(), "logout")}">Logout</div>
        `;

        this.loginAndRegister = `            
            <a id="login" data-nav="nav" class="ui item" href="/login">Login</a>
            <a id="register" data-nav="nav" class="ui item" href="/register">Register</a>
        `;

        this.innerHTML = `
            <header class="ui secondary menu">
                <a data-nav="nav" class="item" href="/">Home</a>
                <div id="nav-right-menu" class="right menu"></div>
            </header>
        `;

        $('#nav-right-menu').append(this.loginAndRegister);

        if (userService.isLoggedIn()) {
            $('#nav-right-menu').append(this.userActions);
            $('#register').remove();
            $('#login').remove();
        }
    }

    clearUser() {
        userService.logout();
        window.Components.header.render();
        redirectUrl('/login');
    }
}

customElements.define('header-custom', Header);
