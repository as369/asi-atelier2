import InputCustom from "./input.js"
import HTMLBindableElement from "./abstract/HTMLBindableElement.js";
import { formatCard } from './card.functions.js';
class FullCard extends HTMLBindableElement {
    constructor(card, label, isSell = false) {
        super();
        this.card = card;
        this.label = label;
        this.isSell = isSell;
    }

    render() {   
        const label = _.flow([
            _.toLower,
            _.capitalize
        ])(this.label)
        const button = $('#sellButton');

        if (_.isEmpty(this.card)) { return; }

        this.innerHTML = `
            <div id="cardDetails"></div>
            <template id="selectedCard">
                <div class="ui card">
                    <div class="content">
                        {{name}}
                    </div>
                    <div class="image">
                        <img src="{{srcImage}}">
                    </div>
                    <div class="content">
                        <div class="description">
                            {{description}}
                        </div>
                    </div>
                    <div class="ui grid extra content">
                        <div class="two column row">
                            <a class="column center aligned">
                                <i class="heart icon"></i>
                                {{hp}}
                            </a>
                            <a class="column center aligned">
                                <i class="certificate icon"></i>
                                {{energy}}
                            </a>
                        </div>
                        <div class="two column row">
                            <a class="column center aligned">
                                <i class="shield alternate icon"></i>
                                {{defence}}
                            </a>
                            <a class="column center aligned">
                                <i class="star icon"></i>
                                {{attack}}
                            </a>
                        </div>
                    </div>
                    <div class="content column center aligned" id="sellButton" onclick="${this.bind(() => this.onButtonClick(),"onCardButtonClick")}">
                        <button class="ui button">
                            <i class="money icon"></i>
                            ${label} card
                        </button>
                    </div>
                </div>
            </template>
        `;
        
        this.addDataToCard({ data: this.card });

        if (!this.isSell) {
            button.addClass("hidden");
        }

        this.button = button.html(new InputCustom("sell", "number", "Price"));
        button.append(this.button);
    }
        
    addDataToCard({ data }) {
        const card = _.get(data, 'card');
        const formatedCard = formatCard({ card });
        const { name, description, attack, defense, energy, hp } = formatedCard;
        const cardDetails = $('#cardDetails');
        let clone = $('#selectedCard').html();
    
        const newContent = clone
            .replace(/{{name}}/g, _.isEmpty(name) ? 'Default' : name)
            .replace(/{{srcImage}}/g, 'https://cdn.animenewsnetwork.com/thumbnails/fit600x1000/cms/feature/89858/01.jpg')
            .replace(/{{description}}/g, _.isEmpty(description) ? 'Default' : description)
            .replace(/{{hp}}/g, !_.isInteger(hp) ? 50 : hp)
            .replace(/{{energy}}/g, !_.isInteger(energy) ? 50 : energy)
            .replace(/{{defence}}/g, !_.isInteger(defense) ? 50 : defense)
            .replace(/{{attack}}/g, !_.isInteger(attack) ? 50 : attack)
        clone = newContent;
    
        cardDetails.empty();
        cardDetails.append(clone);
    }

    bind(callback, name) {
        if (!callback) { return; }

        window[name] = callback;
        return `window.${name}();`
    }

    bindOnButtonClick(callback) {
        this.callback = callback;
    }

    onButtonClick() {
        if(!this.callback) { return; }

        this.callback();
    }

    setCard({ card }) {
        this.card = card;
        this.render();
    }  
}

customElements.define('full-card', FullCard);

export default FullCard;    