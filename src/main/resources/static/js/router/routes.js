export default {
    404: {
        template: '/templates/404.html',
        title: 'Not found',
        description: 'The current page does not exist',
        secure: false,
    },
    '/': {
        template: '/templates/home.html',
        title: 'Home',
        description: 'This is the home page',
        secure: true,
    },
    '/login': {
        template: '/templates/login.html',
        title: 'Login',
        description: 'This is the login page',
        script: '/js/pages/login.js',
        secure: false,
    },
    '/register': {
        template: '/templates/register.html',
        title: 'Register',
        description: 'This is the register page',
        script: '/js/pages/register.js',
        secure: false,
    },
    '/buy': {
        template: '/templates/buy.html',
        title: 'Buy',
        description: 'This is the buy page',
        script: '/js/pages/buy.js',
        secure: true,
    },
    '/sell': {
        template: '/templates/sell.html',
        title: 'Sell',
        description: 'This is the login page',
        script: '/js/pages/sell.js',
        secure: true,
    },
    '/play': {
        template: '/templates/play.html',
        title: 'Play',
        description: 'This is the play page',
        script: '/js/pages/play.js',
        secure: true,
    },
};
