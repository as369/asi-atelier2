import Store from "../utils/store.js"; 
import API from "../utils/axios.js";
import parseJwt from "../utils/jwt.js";

const user_key = 'user';
const rootPath = 'api/public/user';

// Bearer

const get_user = () => Store.getItem({ key: user_key });
const set_user = ({ user }) => Store.setItem({ key: user_key, item: user });

function setCookieToken({ token }) {
    document.cookie = `token=${token}`;
}

export default {
    async getUser({ token }) {
        const jwtData = parseJwt({ token });
        const mail = _.get(jwtData, 'sub'); // not getting mail
        const response = await API.post('api/secure/user/mail', { mail });
        const user = _.get(response, 'data');
        return user;
    },
    
    async register(data) {
        const path = `${rootPath}/signup`;

        try {
            const response = await API.post(path, data);
            const token = _.get(response, 'data');
            if (_.isEmpty(token)) {
                return false;
            }
            const user = this.getUser({ token });
            setCookieToken({ token });
            set_user({ user });
            return true;
        } catch (error) {
            console.error(error)
            return false;
        }
    },
    
    async login(data) {
        const path = `${rootPath}/signin`;

        try {
            const response = await API.post(path, data);
            const token = _.get(response, 'data');
            if (_.isEmpty(token)) {
                return false;
            }
            const user = this.getUser({ token });
            setCookieToken({ token });
            set_user({ user });
            return true;
        } catch (error) {
            console.error(error)
            return false;
        }
    },

    async logout() {
        try {
            Store.removeItem({ key: user_key });
            document.cookie = "token=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
            return true;
        } catch (error) {
            return false;
        }
    },
    get_user,
    set_user,
    isLoggedIn() {
        return !_.isNull(get_user());
    }
}