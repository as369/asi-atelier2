import userService from "./userService.js"
import API from "../utils/axios.js";
import cardService from "./cardService.js";

const rootPath = 'api/public/cardInstances';

export default {
    async buyCard({ idCardInstance }) {
        const user = await userService.getUser({ token: document.cookie });
        const bodyRequest = {
            "idUser": user.id_user,
            "idCardInstance": idCardInstance,
            "date": "2007-12-03T10:15:30",
            "price": 1000.00
        }
        const response = await API.post('api/private/buy', bodyRequest);
        return response
    },
    async sellCard({ idCardInstance }) {
        const user = await userService.getUser({ token: document.cookie });
        const bodyRequest = {
            "idUser": user.id_user,
            "idCardInstance": idCardInstance,
            "date": "2007-12-03T10:15:30",
            "price": 1000.00
        }
        const response = await API.post('api/private/sales', bodyRequest);
        return response
    },
    async getCardsToSell() {
        const path = `${rootPath}/sale`
        return await API.get(path);
    }
}