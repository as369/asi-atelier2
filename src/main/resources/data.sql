INSERT INTO family (name_family, id_family)
VALUES
  ('TestOne', 100),
  ('TestTwo', 101);

INSERT INTO card (energy_card, attack_card, defense_card, description_card, name_card, hp_card, id_family, id_card)
VALUES
  (10, 11, 12, 'CardOne', 'Number One', 100, 100, 100),
  (20, 21, 22, 'CardTwo', 'Number Two', 200, 100, 101),
  (30, 31, 32, 'CardThree', 'Number Three', 300, 101, 102),
  (40, 41, 42, 'CardFour', 'Number Four', 400, 101, 103),
  (50, 51, 52, 'CardFive', 'Number Five', 500, 100, 104),
  (60, 61, 62, 'CardSix', 'Number Six', 600, 101, 105);