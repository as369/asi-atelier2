  package com.sp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sp.dto.CardDTO;
import com.sp.mapper.CardMapper;
import com.sp.service.CardService;
import com.sp.service.FamilyService;

@RequestMapping("/api")
@RestController
public class CardController {
	@Autowired
	CardService cardService;
	@Autowired
	FamilyService familyService;
	@Autowired
	CardMapper cardMapper;
	
	// GET 
	@GetMapping(value="/public/cards")
	public List<CardDTO> getAllCards(){
		return cardMapper.toDTOList(cardService.getAllCards());
	}
	
	@GetMapping(value="/public/cards/{id_card}")
	public CardDTO getCardbById(@PathVariable Integer id_card){
		return cardMapper.toDTO(cardService.getCardById(id_card));
	}
	
	@GetMapping(value="/public/cards/family/{id_family}")
	public List<CardDTO> getCardsbByIdFamily(@PathVariable Integer id_family){
		return cardMapper.toDTOList(cardService.getCardsByFamily(familyService.getFamilyById(id_family)));
	}
	
	// POST
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping(value="/private/cards")
	public void createCard(@RequestBody CardDTO dto){
		cardService.createCard(cardMapper.toModel(dto, familyService));
	}
}

