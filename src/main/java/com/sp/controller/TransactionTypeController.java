package com.sp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sp.dto.TransactionTypeDTO;
import com.sp.mapper.TransactionTypeMapper;
import com.sp.service.TransactionTypeService;

@RequestMapping("/api")
@RestController
public class TransactionTypeController {
	@Autowired
	TransactionTypeService transactionTypeService;
	@Autowired
	TransactionTypeMapper transactionTypeMapper;
	
	// GET 
	@GetMapping(value="/public/transactionTypes")
	public List<TransactionTypeDTO> getAllTransactionTypes(){
		return transactionTypeMapper.toDTOList(transactionTypeService.getAllTransactionTypes());
	}
	
	@GetMapping(value="/public/transactionTypes/{id_transaction_type}")
	public TransactionTypeDTO getTransactionTypeById(@PathVariable int id_transaction_type) {
		return transactionTypeMapper.toDTO(transactionTypeService.getTransactionTypeById(id_transaction_type));
	}
}
