package com.sp.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sp.dto.UserDTO;
import com.sp.dto.UserRegisterDTO;
import com.sp.mapper.UserMapper;
import com.sp.mapper.UserRegisterMapper;
import com.sp.service.UserService;
import com.sp.utils.JWTUtils;

@RestController
@RequestMapping("/api")
public class UserController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserMapper userMapper;
	
	@Autowired
	UserRegisterMapper userRegisterMapper;
	
	
	@GetMapping("/user/{id_user}")
	public UserDTO getUserById(@PathVariable Integer id_user) {
		return userMapper.toDTO(userService.getUserById(id_user));
	}

	@PostMapping("secure/user/mail")
	public UserDTO getUserByMail(@RequestBody UserRegisterDTO userRegisterDTO) {
		return userMapper.toDTO(userService.getUserByMail(userRegisterDTO.getMail()));
	}
	
	// Inscription
	@PostMapping("/public/user/signup")
	public void addUser(@RequestBody UserRegisterDTO userRegisterDTO, HttpServletResponse response) throws IOException {
		userService.addUser(userRegisterMapper.toModel(userRegisterDTO), response);
	}
	// Connexion
	@PostMapping("/public/user/signin")
	public void loginUser(@RequestBody UserRegisterDTO userRegisterDTO, HttpServletResponse response) throws IOException {
		userService.loginUser(userRegisterMapper.toModel(userRegisterDTO), response);
	}
	
	// test autorisation
	@GetMapping("/secure/user/test")
	public String testAuth(String token) {
	
		//JWTUtils.parseJWT(token.split(" ")[1]);
		return "t'es login";
		
	}

	// test autorisation
	@GetMapping("/admin/user/test")
	public String testAdmin(String token) {
	
		//JWTUtils.parseJWT(token.split(" ")[1]);
		return "t'es admin";
		
	}
}
