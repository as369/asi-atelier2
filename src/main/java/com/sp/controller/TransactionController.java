package com.sp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sp.dto.TransactionDTO;
import com.sp.mapper.TransactionMapper;
import com.sp.service.CardInstanceService;
import com.sp.service.TransactionService;
import com.sp.service.TransactionTypeService;
import com.sp.service.UserService;

@RequestMapping("/api")
@RestController
public class TransactionController {
	@Autowired
	TransactionService transactionService;
	@Autowired
	UserService userService;
	@Autowired
	CardInstanceService cardInstanceService;
	@Autowired
	TransactionTypeService transactionTypeService;
	@Autowired
	TransactionMapper transactionMapper;
	
	// GET 
	@GetMapping(value="/public/transactions")
	public List<TransactionDTO> getAllTransactions(){
		return transactionMapper.toDTOList(transactionService.getAllTransactions());
	}
	
	@GetMapping(value="/public/transactions/{id_transaction}")
	public TransactionDTO getTransactionById(@PathVariable int id_transaction) {
		return transactionMapper.toDTO(transactionService.getTransactionById(id_transaction));
	}
	
	@GetMapping(value="/secure/transactions/user/{id_user}")
	public List<TransactionDTO> getTransactionsByUser(@PathVariable int id_user) {
		return transactionMapper.toDTOList(transactionService.getTransactionsByUser(userService.getUserById(id_user)));
	}
	
	// POST
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping(value="/private/transactions")
	public void createTransaction(@RequestBody TransactionDTO transactionDTO) {
		transactionService.createTransaction(transactionMapper.toModel(transactionDTO, cardInstanceService, userService, transactionTypeService));
	}
	
	// UPDATE
	@ResponseStatus(code = HttpStatus.OK)
	@PutMapping(value="/private/transactions")
	public void updateTransaction(@RequestBody TransactionDTO transactionDTO) {
		transactionService.updateTransaction(transactionMapper.toModel(transactionDTO, cardInstanceService, userService, transactionTypeService));
	}
	
}