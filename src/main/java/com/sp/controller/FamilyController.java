package com.sp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sp.dto.FamilyDTO;
import com.sp.mapper.FamilyMapper;
import com.sp.service.FamilyService;

@RequestMapping("/api")
@RestController
public class FamilyController {
	@Autowired
	FamilyService familyService;
	
	@Autowired
	FamilyMapper familyMapper;
	
	// GET 
	@GetMapping(value="/public/families")
	public List<FamilyDTO> getAllFamilies(){
		return familyMapper.toDTOList(familyService.getAllFamilies());
	}

	@GetMapping(value = "/public/families/{id_family}")
	public FamilyDTO getFamilyById(@PathVariable int id_family) {
		return familyMapper.toDTO(familyService.getFamilyById(id_family));
	}
	
	// POST
	@ResponseStatus(code = HttpStatus.CREATED)
	@PostMapping(value = "/private/families")
	public void createFamily(@RequestBody FamilyDTO dto) {
		familyService.createFamily(familyMapper.toModel(dto));
	}	
}



