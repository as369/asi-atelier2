package com.sp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sp.dto.CardInstanceDTO;
import com.sp.mapper.CardInstanceMapper;
import com.sp.service.CardInstanceService;
import com.sp.service.CardService;
import com.sp.service.UserService;


@RequestMapping("/api")
@RestController
public class CardInstanceController {
	@Autowired
	CardInstanceService cardInstanceService;
	@Autowired
	CardService cardService;
	@Autowired
	UserService userService;
	@Autowired
	CardInstanceMapper cardInstanceMapper;
	
	// GET 
	@GetMapping(value="/public/cardInstances")
	public List<CardInstanceDTO> getAllCardInstances(){
		return cardInstanceMapper.toDTOList(cardInstanceService.getAllCardInstances());
	}
	
	@GetMapping(value="/public/cardInstances/{id_card_instance")
	public CardInstanceDTO getCardInstanceById(@PathVariable int id_card_instance) {
		return cardInstanceMapper.toDTO(cardInstanceService.getCardInstanceById(id_card_instance));
	}
	
	@GetMapping(value="/secure/cardInstances/user/{id_user}")
	public List<CardInstanceDTO> getCardInstancesByIdUser(@PathVariable int id_user) {
		return cardInstanceMapper.toDTOList(cardInstanceService.getCardInstancesByUser(userService.getUserById(id_user)));
	}
	
	@GetMapping(value="/public/cardInstances/sale")
	public List<CardInstanceDTO> getCardInstancesForSale() {
		return cardInstanceMapper.toDTOList(cardInstanceService.getCardInstancesForSale());
	}
	
	// POST
	@PostMapping(value="/private/cardInstances")
	public void createCardInstance(@RequestBody CardInstanceDTO dto) {
		cardInstanceService.createCardInstance(cardInstanceMapper.toModel(dto, cardService, userService));
	}
}