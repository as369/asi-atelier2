package com.sp.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
public class CardInstance {
	
	@Id
	@GeneratedValue
	@Column(columnDefinition = "serial")
	private Integer id_instance;
	
	@ManyToOne
	@JoinColumn(name = "id_card", foreignKey = @ForeignKey(name = "fk_card_id_card"))
	private Card card;
	
	@ManyToOne
	@JoinColumn(columnDefinition = "integer", name = "id_user", foreignKey = @ForeignKey(name = "fk_user_is_user"))
	private User user;
	
	@OneToMany(mappedBy = "cardInstance")
	private Set<Transaction> sales = new HashSet<>();

	public CardInstance() {
		
	}

	public Integer getId_instance() {
		return id_instance;
	}

	public CardInstance setId_instance(Integer id_instance) {
		this.id_instance = id_instance;
		return this;
	}

	public Card getCard() {
		return card;
	}

	public CardInstance setCard(Card card) {
		this.card = card;
		return this;
	}

	public User getUser() {
		return user;
	}

	public CardInstance setUser(User user) {
		this.user = user;
		return this;
	}
	

}
