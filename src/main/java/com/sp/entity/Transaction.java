package com.sp.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Transaction {
	
	@Id
	@GeneratedValue
	@Column(columnDefinition = "serial")
	private Integer id_sale;
	@Column(name="price_sale")
	private String price;
	@Column(name="date_sale")
	private LocalDateTime date;
	
	@ManyToOne
	@JoinColumn(name = "id_card_instance", foreignKey = @ForeignKey(name = "fk_card_instance_id_card_instance"))
	private CardInstance cardInstance;
	
	@ManyToOne
	@JoinColumn(name = "id_user", foreignKey = @ForeignKey(name = "fk_user_id_user"))
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "id_transaction_type", foreignKey = @ForeignKey(name = "fk_transaction_type_id_transaction_type"))
	private TransactionType transactionType;
	

	public Transaction() {

	}

	public Integer getId_sale() {
		return id_sale;
	}

	public Transaction setId_sale(Integer id_sale) {
		this.id_sale = id_sale;
		return this;
	}

	public String getPrice() {
		return price;
	}

	public Transaction setPrice(String price) {
		this.price = price;
		return this;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public Transaction setDate(LocalDateTime date) {
		this.date = date;
		return this;
	}

	public CardInstance getCardInstance() {
		return cardInstance;
	}

	public Transaction setCardInstance(CardInstance cardInstance) {
		this.cardInstance = cardInstance;
		return this;
	}

	public User getUser() {
		return user;
	}

	public Transaction setUser(User user) {
		this.user = user;
		return this;
	}

	public TransactionType getTransactionType() {
		return transactionType;
	}

	public Transaction setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
		return this;
	}
}
