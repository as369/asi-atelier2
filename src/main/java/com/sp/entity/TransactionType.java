package com.sp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class TransactionType {
	
	@Id
	@GeneratedValue
	private Integer id_transaction_type;
	@Column(name="name_transaction_type")
	private String name;
	

	public TransactionType() {
		
	}
	
	public Integer getId_transaction_type() {
		return id_transaction_type;
	}
	public void setId_transaction_type(Integer id_transaction_type) {
		this.id_transaction_type = id_transaction_type;
	}
	public String getName() {
		return name;
	}
	public TransactionType setName(String name) {
		this.name = name;
		return this;
	}	
}
