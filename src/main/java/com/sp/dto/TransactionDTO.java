package com.sp.dto;

import java.time.LocalDateTime;

public class TransactionDTO {
	private String price;
	private LocalDateTime date;
	private int idCardInstance;
	private int idUser;
	private int idTransactionType;
	
	public TransactionDTO() {
	}
	
	public String getPrice() {
		return price;
	}
	
	public TransactionDTO setPrice(String price) {
		this.price = price;
		return this;
	}
	
	public LocalDateTime getDate() {
		return date;
	}
	
	public TransactionDTO setDate(LocalDateTime date) {
		this.date = date;
		return this;
	}
	
	public int getIdCardInstance() {
		return idCardInstance;
	}
	
	public TransactionDTO setIdCardInstance(int idCardInstance) {
		this.idCardInstance = idCardInstance;
		return this;
	}
	
	public int getIdUser() {
		return idUser;
	}
	
	public TransactionDTO setIdUser(int idUser) {
		this.idUser = idUser;
		return this;
	}

	public int getIdTransactionType() {
		return idTransactionType;
	}

	public TransactionDTO setIdTransactionType(int idTransactionType) {
		this.idTransactionType = idTransactionType;
		return this;
	}
	
}
