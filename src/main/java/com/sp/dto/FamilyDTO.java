package com.sp.dto;

public class FamilyDTO {
	private Integer idFamily;
	private String name;

	public Integer getIdFamily() {
		return idFamily;
	}

	public FamilyDTO setIdFamily(Integer idFamily) {
		this.idFamily = idFamily;
		return this;
	}

	public FamilyDTO() {
	}
	
	public String getName() {
		return name;
	}
	
	public FamilyDTO setName(String name) {
		this.name = name;
		return this;
	}
}