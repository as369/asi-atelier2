package com.sp.dto;

public class TransactionTypeDTO {
	private String name;

	public TransactionTypeDTO() {
	}
	
	public String getName() {
		return name;
	}

	public TransactionTypeDTO setName(String name) {
		this.name = name;
		return this;
	}
	
	
}
