package com.sp.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sp.entity.Card;

public class CardInstanceDTO {
	private Integer id_instance;
	private Card card;
	private Integer idUser;
	
	public Card getCard() {
		return card;
	}
	
	public CardInstanceDTO setCard(Card card) {
		this.card = card;
		return this;
	}
	
	@JsonIgnore
	public int getIdUser() {
		return idUser;
	}
	
	public CardInstanceDTO setIdUser(Integer idUser) {
		this.idUser = idUser;
		return this;
	}

	public Integer getId_instance() {
		return id_instance;
	}

	public CardInstanceDTO setId_instance(Integer id_instance) {
		this.id_instance = id_instance;
		return this;
	}
}
