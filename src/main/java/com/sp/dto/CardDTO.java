package com.sp.dto;

public class CardDTO {
	private int idCard;
	private int energyCard;
	private int hpCard;
	private int attackCard;
	private int defenseCard;
	private String nameCard;
	private String descriptionCard;
	private Integer idFamily;
	
	public CardDTO() {
	}

	public int getIdCard() {
		return idCard;
	}

	public CardDTO setIdCard(int idCard) {
		this.idCard = idCard;
		return this;
	}

	public int getEnergyCard() {
		return energyCard;
	}

	public CardDTO setEnergyCard(int energyCard) {
		this.energyCard = energyCard;
		return this;
	}

	public int getHpCard() {
		return hpCard;
	}

	public CardDTO setHpCard(int hpCard) {
		this.hpCard = hpCard;
		return this;
	}

	public int getAttackCard() {
		return attackCard;
	}

	public CardDTO setAttackCard(int attackCard) {
		this.attackCard = attackCard;
		return this;
	}

	public int getDefenseCard() {
		return defenseCard;
	}

	public CardDTO setDefenseCard(int defenseCard) {
		this.defenseCard = defenseCard;
		return this;
	}

	public String getNameCard() {
		return nameCard;
	}

	public CardDTO setNameCard(String nameCard) {
		this.nameCard = nameCard;
		return this;
	}

	public String getDescriptionCard() {
		return descriptionCard;
	}

	public CardDTO setDescriptionCard(String descriptionCard) {
		this.descriptionCard = descriptionCard;
		return this;
	}

	public Integer getIdFamily() {
		return idFamily;
	}

	public CardDTO setIdFamily(Integer idFamily) {
		this.idFamily = idFamily;
		return this;
	}
}