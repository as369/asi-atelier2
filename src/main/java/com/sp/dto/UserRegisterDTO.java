package com.sp.dto;

public class UserRegisterDTO {
	
	private Boolean isAdmin;
	private Double money;
	private String surname;
	private String name;
	private String password;
	private String mail;
	
	public UserRegisterDTO() {
		super();
	}

	public UserRegisterDTO(Boolean isAdmin, Double money, String surname, String name, String password, String mail) {
		super();
		this.isAdmin = isAdmin;
		this.money = money;
		this.surname = surname;
		this.name = name;
		this.password = password;
		this.mail = mail;
	}
	
	public Boolean getIsAdmin() {
		return isAdmin;
	}
	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	
	

}
