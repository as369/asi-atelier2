package com.sp.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.sp.dto.CardInstanceDTO;
import com.sp.entity.CardInstance;
import com.sp.service.CardService;
import com.sp.service.UserService;

@Component
public class CardInstanceMapper {
	public CardInstance toModel(CardInstanceDTO dto, CardService cardService, UserService userService) {
		return new CardInstance()
				.setCard(dto.getCard())
				.setUser(userService.getUserById(dto.getIdUser()))
				.setId_instance(dto.getId_instance());
		}
	
	public List<CardInstance> toModelList(List<CardInstanceDTO> listDTO, CardService cardService, UserService userService) {
		List<CardInstance> toReturn = new ArrayList<CardInstance>();
		for(CardInstanceDTO cardInstanceDTO:listDTO) {
			toReturn.add(toModel(cardInstanceDTO, cardService, userService));
		}
		return toReturn;
	}
	
	public CardInstanceDTO toDTO(CardInstance cardInstance) {
		return new CardInstanceDTO()
				.setCard(cardInstance.getCard())
				.setIdUser(cardInstance.getUser() == null ? null : cardInstance.getUser().getId_user())
				.setId_instance(cardInstance.getId_instance());
		}
	
	public List<CardInstanceDTO> toDTOList(List<CardInstance> listCardInstance) {
		List<CardInstanceDTO> toReturn = new ArrayList<CardInstanceDTO>();
		for(CardInstance cardInstance:listCardInstance) {
			toReturn.add(toDTO(cardInstance));
		}
		return toReturn;
	}
}
