package com.sp.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.sp.dto.CardDTO;
import com.sp.entity.Card;
import com.sp.service.FamilyService;

@Component
public class CardMapper {
	public Card toModel(CardDTO dto, FamilyService familyService) {
		return new Card()
				.setEnergyCard(dto.getEnergyCard())
				.setHpCard(dto.getHpCard())
				.setAttackCard(dto.getAttackCard())
				.setDefenseCard(dto.getDefenseCard())
				.setNameCard(dto.getNameCard())
				.setFamily(familyService.getFamilyById(dto.getIdFamily()));
		}
	
	public List<Card> toModelList(List<CardDTO> listDTO, FamilyService familyService) {
		List<Card> toReturn = new ArrayList<Card>();
		for(CardDTO cardDTO:listDTO) {
			toReturn.add(toModel(cardDTO, familyService));
		}
		return toReturn;
	}
	
	public CardDTO toDTO(Card card) {
		return new CardDTO()
				.setIdCard(card.getId_card())
				.setEnergyCard(card.getEnergyCard())
				.setHpCard(card.getHpCard())
				.setAttackCard(card.getAttackCard())
				.setDefenseCard(card.getDefenseCard())
				.setDescriptionCard(card.getDescriptionCard())
				.setNameCard(card.getNameCard())
				.setIdFamily(card.getFamily().getId_family());
		}
	
	public List<CardDTO> toDTOList(List<Card> listCard) {
		List<CardDTO> toReturn = new ArrayList<CardDTO>();
		for(Card card:listCard) {
			toReturn.add(toDTO(card));
		}
		return toReturn;
	}
}
