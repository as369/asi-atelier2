package com.sp.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.sp.dto.TransactionTypeDTO;
import com.sp.entity.TransactionType;

@Component
public class TransactionTypeMapper {
	public TransactionType toModel(TransactionTypeDTO dto) {
		return new TransactionType()
				.setName(dto.getName());
	}
	
	public List<TransactionType> toModelList(List<TransactionTypeDTO> listDTO) {
		List<TransactionType> toReturn = new ArrayList<TransactionType>();
		for(TransactionTypeDTO transactionTypeDTO:listDTO) {
			toReturn.add(toModel(transactionTypeDTO));
		}
		return toReturn;
	}
	
	public TransactionTypeDTO toDTO(TransactionType transactionType) {
		return new TransactionTypeDTO()
				.setName(transactionType.getName());
	}
	
	public List<TransactionTypeDTO> toDTOList(List<TransactionType> listTransactionType) {
		List<TransactionTypeDTO> toReturn = new ArrayList<TransactionTypeDTO>();
		for(TransactionType transactionType:listTransactionType) {
			toReturn.add(toDTO(transactionType));
		}
		return toReturn;
	}
}
