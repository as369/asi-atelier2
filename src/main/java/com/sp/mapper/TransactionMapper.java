package com.sp.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.sp.dto.TransactionDTO;
import com.sp.entity.Transaction;
import com.sp.service.CardInstanceService;
import com.sp.service.TransactionTypeService;
import com.sp.service.UserService;

@Component
public class TransactionMapper {
	public Transaction toModel(TransactionDTO dto, CardInstanceService cardInstanceService, UserService userService, TransactionTypeService transactionTypeService) {
		return new Transaction()
				.setPrice(dto.getPrice())
				.setDate(dto.getDate())
				.setCardInstance(cardInstanceService.getCardInstanceById(dto.getIdCardInstance()))
				.setUser(userService.getUserById(dto.getIdUser()))
				.setTransactionType(transactionTypeService.getTransactionTypeById(dto.getIdTransactionType()));
	}
	
	public List<Transaction> toModelList(List<TransactionDTO> listDTO, CardInstanceService cardInstanceService, UserService userService, TransactionTypeService transactionTypeService) {
		List<Transaction> toReturn = new ArrayList<Transaction>();
		for(TransactionDTO transactionDTO:listDTO) {
			toReturn.add(toModel(transactionDTO, cardInstanceService, userService, transactionTypeService));
		}
		return toReturn;
	}
	
	public TransactionDTO toDTO(Transaction transaction) {
		return new TransactionDTO()
				.setDate(transaction.getDate())
				.setIdCardInstance(transaction.getCardInstance().getId_instance())
				.setIdUser(transaction.getUser().getId_user())
				.setPrice(transaction.getPrice())
				.setIdTransactionType(transaction.getTransactionType().getId_transaction_type());
	}	
	
	public List<TransactionDTO> toDTOList(List<Transaction> listTransaction) {
		List<TransactionDTO> toReturn = new ArrayList<TransactionDTO>();
		for(Transaction transaction:listTransaction) {
			toReturn.add(toDTO(transaction));
		}
		return toReturn;
	}
}


