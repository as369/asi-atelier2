package com.sp.filter;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.sp.utils.JWTUtils;

import io.jsonwebtoken.Claims;


@Component
public class RouteFilter extends OncePerRequestFilter {

	private String publicRoute = "/api/public";
	private String secureRoute = "/api/secure";
	private String adminRoute = "/api/admin";

	
	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		return true;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		String token = null;
		Cookie[] cookies = request.getCookies();
		
		if (cookies != null) {
			token = Arrays.stream(cookies).filter(c -> "JWT".equals(c.getName()))
					.findFirst()
					.map(cookie -> cookie.getValue())
					.orElse(null);
			
			if(token == null) {
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "The token is empty");
				// response.setStatus(401);
			} else if (request.getServletPath().startsWith(adminRoute)) {
				Claims claims = JWTUtils.parseJWT(token);
				boolean isAdmin = (boolean) claims.get("isAdmin");
				if(!isAdmin)  {
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "You are not admin");
					// response.setStatus(401);
				}
			}
			
			
			filterChain.doFilter(request, response);
		}
		
	}

}

