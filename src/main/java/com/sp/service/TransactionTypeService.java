package com.sp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.entity.Transaction;
import com.sp.entity.TransactionType;
import com.sp.repository.TransactionTypeRepository;

@Service
public class TransactionTypeService {
	@Autowired
	TransactionTypeRepository transactionTypeRepository;
	
	public List<TransactionType> getAllTransactionTypes() {
		return transactionTypeRepository.findAll();
	}
	
	public TransactionType getTransactionTypeById(Integer id_transaction_type) {
		return transactionTypeRepository.findById(id_transaction_type).orElseThrow(() -> new RuntimeException());
	}
	
}
