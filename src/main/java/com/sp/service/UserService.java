package com.sp.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.sp.entity.CardInstance;
import com.sp.entity.User;
import com.sp.repository.CardInstanceRepository;
import com.sp.repository.UserRepository;
import com.sp.utils.JWTUtils;

@Service
public class UserService {
	@Autowired
	CardService cardService; // Should it be here ?
	@Autowired
	UserRepository userRepository;
	@Autowired
	CardInstanceRepository cardInstanceRepository;
	
	BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

	public HttpServletResponse addUser(User user, HttpServletResponse response) throws IOException {
		// Hash le pwd avev bcrypt
        String hashedPassword = bCryptPasswordEncoder.encode(user.getPassword());
        user.setPassword(hashedPassword);
		
        Map<String, Object> map = new HashMap<>();
		map.put("isAdmin", user.getIsAdmin());
		
		String token = JWTUtils.createJWT(user.getMail(), map);
		userRepository.save(user);

		response.getWriter().write(token);
		
		// Adding cards for the new user
		cardInstanceRepository.save(new CardInstance().setCard(cardService.getAllCards().get(0)).setUser(user));
		cardInstanceRepository.save(new CardInstance().setCard(cardService.getAllCards().get(1)).setUser(user));
		cardInstanceRepository.save(new CardInstance().setCard(cardService.getAllCards().get(2)).setUser(user));
		
		return response;
	}
	
	public HttpServletResponse loginUser(User user, HttpServletResponse response) throws IOException {
		Optional<User> userFind = userRepository.findByMail(user.getMail());
		// If the mail isn't known display 'mail unkown'
		
		if (!userFind.isPresent()) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "The mail used doesn't exist");
			return response;
		}

		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		// Password verification
		if(!bCryptPasswordEncoder.matches(user.getPassword(), userFind.get().getPassword())) {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Wrong password for this mail");
			return response;
		}
		// Creation & Gestion of Token
		Map<String, Object> map = new HashMap<>();

		map.put("isAdmin", userFind.get().getIsAdmin());
		
		String token = JWTUtils.createJWT(userFind.get().getMail(), map);
		response.getWriter().write(token);

		return response;
	}


	public User getUserById(Integer idUser) {
		return userRepository.findById(idUser).orElseThrow(() -> new RuntimeException());
	}

	public User getUserByMail(String mail) {
		return userRepository.findByMail(mail).orElseThrow(() -> new RuntimeException());
	}
}
