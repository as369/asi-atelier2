package com.sp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.entity.CardInstance;
import com.sp.entity.Transaction;
import com.sp.entity.User;
import com.sp.repository.TransactionRepository;

@Service
public class TransactionService {
	@Autowired
	TransactionRepository transactionRepository;
	
	public List<Transaction> getAllTransactions() {
		return transactionRepository.findAll();
	}
	
	public List<Transaction> getTransactionsByUser(User user) {
		return transactionRepository.findByUser(user);
	}
	
	public Transaction getTransactionById(Integer id_transaction) {
		return transactionRepository.findById(id_transaction).orElseThrow(() -> new RuntimeException());
	}

	public List<Transaction> getTransactionsByCardInstance(CardInstance cardInstance) {
			return transactionRepository.findByCardInstance(cardInstance);
	}
	
	public void createTransaction(Transaction transaction) {
		transactionRepository.save(transaction);
	}
	
	public void updateTransaction(Transaction transaction) {
		transactionRepository.save(transaction);
	}
}
