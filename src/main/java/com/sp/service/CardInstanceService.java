package com.sp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.entity.CardInstance;
import com.sp.entity.User;
import com.sp.repository.CardInstanceRepository;

@Service
public class CardInstanceService {
	@Autowired
	CardInstanceRepository cardInstanceRepository;

	public List<CardInstance> getAllCardInstances() {
		return cardInstanceRepository.findAll();
	}
	
	public List<CardInstance> getCardInstancesByUser(User user) {
		return cardInstanceRepository.findByUser(user);
	}
	
	public CardInstance getCardInstanceById(Integer idCardInstance) {
		return cardInstanceRepository.findById(idCardInstance).orElseThrow(() -> new RuntimeException());
	}
	
	public List<CardInstance> getCardInstancesForSale() {
		List<CardInstance> toReturn = new ArrayList<CardInstance>();
		List<CardInstance> listAllCardInstance = cardInstanceRepository.findAll();
		for(CardInstance cardInstance:listAllCardInstance) {
			if (cardInstance.getUser() == null) {
				toReturn.add(cardInstance);
			}
		}

		return toReturn;
	}

	public void createCardInstance(CardInstance cardInstance) {
		cardInstanceRepository.save(cardInstance);
	}
}