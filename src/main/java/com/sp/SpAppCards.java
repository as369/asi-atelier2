package com.sp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

// Disable Spring Security
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class SpAppCards {
	
	public static void main(String[] args) {
		SpringApplication.run(SpAppCards.class,args);
	}
}
