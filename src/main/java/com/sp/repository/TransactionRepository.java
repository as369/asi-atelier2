package com.sp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sp.entity.CardInstance;
import com.sp.entity.Transaction;
import com.sp.entity.User;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
	public List<Transaction> findByUser(User user);
	public List<Transaction> findByCardInstance(CardInstance cardInstance);
}


