package com.sp.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sp.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {

	 Optional<User> findByMail(String mail);
	 // public List<User> findByName(String name);
}

