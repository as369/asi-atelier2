package com.sp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sp.entity.CardInstance;
import com.sp.entity.User;

@Repository
public interface CardInstanceRepository extends JpaRepository<CardInstance, Integer>{
	public List<CardInstance> findByUser(User user);

}
