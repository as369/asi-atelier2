package com.sp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sp.entity.Card;
import com.sp.entity.Family;

@Repository
public interface CardRepository extends JpaRepository<Card, Integer> {
	public List<Card> findByFamily(Family family);
}
